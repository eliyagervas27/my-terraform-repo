provider "azurerm" {
  features {}
}

module "resourcegroup_module_deploy" {
    source = "../modules/rg"
    resource_group_name = "rg_demo"
    resource_group_location = "eastus2"
}
