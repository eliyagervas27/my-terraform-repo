provider "azurerm" {
  features {}
}

module "resourcegroup_module_deploy" {
    source = "../modules/rg"
    resource_group_name = "rg_demo2"
    resource_group_location = "eastus2"
}

module "southafrica_north" {
   source = "../modules/db"
   resource_group_name = "module.resourcegroup_module_deploy.name"
   resource_group_location = "module.resourcegroup_module_deploy.location"
   storage_account_name = "newdatabase"

}

