terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
}

#--variables---
variable resource_group_name {
   type = string
   default = "remote-state-rg"
}

variable location {
   type = string
   default = "southafricanorth"
}

variable storage_account_name {
    type = string
    default = "remotestatestorage"
}

#---resources----
resource "azurerm_resource_group" "ResourceGroup" {
    name = var.resource_group_name
    location = var.location
}

resource "azurerrm_storage_account" "StorageAccount" {
    resource_group_name = azurerm_resource_group.ResourceGroup.name
    name = var.storage_account_name
    location = azurerm_resource_group.ResourceGroup.location
    account_tier = "Standard"
    account_replication_type = "LRS"
}

resource "azurerm_storage_container" "StorageContainer" {
    storage_account_name  = azurerrm_storage_account.StorageAccount.name
    name = "terraform-state"
    
}

#-----data------
data "azurerm_storage_account_sas" "state" {
  connection_string = azurerm_storage_account.sa.primary_connection_string
  https_only        = true

  resource_types {
    service   = true
    container = true
    object    = true
  }

  services {
    blob  = true
    queue = false
    table = false
    file  = false
  }

  start  = timestamp()
  expiry = timeadd(timestamp(), "17520h")

  permissions {
    read    = true
    write   = true
    delete  = true
    list    = true
    add     = true
    create  = true
    update  = false
    process = false
  }
}

#-----local files -----

resource "local_file" "post-config" {
     depends_on = [azurerm_storage_container.StorageContainer]
     filename = "${path.module}/backend-config.txt"

     content  = <<EOF
storage_account_name = "${azurerm_storage_account.sa.name}"
container_name = "terraform-state"
key = "terraform.tfstate"
sas_token = "${data.azurerm_storage_account_sas.state.sas}"

     EOF
}

# -----Output-----
output "resource_group_name" {
   value = azurerm_resource_group.ResourceGroup.name
}

output "storage_account_name"{
    value = azurerm_storage_account.StorageAccount.name
} 
output "storage_account_container" {
     value = azurerm_storage_container.StorageContainer.name
}
