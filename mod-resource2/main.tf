terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
}


# Resources
resource "azurerm_resource_group" "rgroup" {
     name = "res-group"
     location ="eastus"
  
}

module "East-Us" {
   source = "../modules"
   resource_group_name = azurerm_resource_group.rgroup.name
   resource_group_location = azurerm_resource_group.rgroup.location

   storage_account_name = "eastusdb"

}
