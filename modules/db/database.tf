resource "azurerm_storage_account" "deep_dive" {
      name = var.storage_account_name
      location = var.resource_group_location
      resource_group_name = var.resource_group_name
      account_replication_type = "LRS"
      account_tier = "Standard"
      account_kind = "StorageV2"
      min_tls_version = "TLS1_2"
      enable_https_traffic_only = true

         
}
